using br.com.franciscorodrigues.mestrado.ETL.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace br.com.franciscorodrigues.mestrado.ETL.Controller
{
    public abstract class ExtractSourceControl : IDisposable
    {
        bool throwsOnError = false;

        public bool ThrowsOnError
        {
            get { return throwsOnError; }
            set { throwsOnError = value; }
        }
        string user;
        public string User
        {
            get { return user; }
            set { user = value; }
        }
        string password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        string rootPath;
        public string RootPath
        {
            get { return rootPath; }
            set { rootPath = value; }
        }

        string targetPath;
        public string TargetPath 
        {
            get { return targetPath; }
            set { targetPath = value; }
        }
        protected string RootLocation
        {
            get { return this.RootPath + this.TargetPath; }
        }
        TimeSpan timeOut = new TimeSpan(0, 0, 30);
        public TimeSpan TimeOut 
        {
            get { return this.timeOut; }
            set { this.timeOut = value; }
        }
        private ReportProgress reportProgress;
        public event ReportProgress OnReportProgress
        {
            add
            {
                reportProgress += value;
            }
            remove
            {
                reportProgress -= value;
            }
        }
        protected virtual void ReportProgress(string format, params object[] args)
        {
            ReportProgress(string.Format(format, args));
        }

        protected virtual void OnLoadUser(string user)
        {
            
            if (this.onLoadUserComplete != null)
            {
                onLoadUserComplete(new UserEventArgs(user));
            }
        }
        protected virtual void ReportProgress(string message)
        {
            LogUtil.Info(message);
            if (this.reportProgress != null)
            {
                reportProgress(message);
            }
        }

        protected void LoadItemComplete(string filePath, int itemId)
        {
            this.LoadItemComplete(new CodeEventArgs(filePath, itemId));
        }
        protected void LoadItemComplete(CodeEventArgs e)
        {
            e.ThrowsOnError = this.ThrowsOnError;
            e.TimeOut = this.TimeOut;
            if (onLoadItemComplete != null)
            {
                onLoadItemComplete(e);
            }
        }

        LoadUserCompleteEventArgs onLoadUserComplete;
        public event LoadUserCompleteEventArgs OnLoadUserComplete
        {
            add
            {
                onLoadUserComplete += value;
            }
            remove
            {
                onLoadUserComplete -= value;

            }
        }

        LoadItemCompleteEventArgs onLoadItemComplete;
        public event LoadItemCompleteEventArgs OnLoadItemComplete
        {
            add
            {
                onLoadItemComplete += value;
            }
            remove
            {
                onLoadItemComplete -= value;
            }
        }

        string tempPath = System.IO.Path.GetTempPath();
        /// <summary>
        /// Default path is <seealso cref="System.IO.Path.GetTempPath" />
        /// </summary>
        public string TempPath
        {
            get { return tempPath; }
            set { tempPath = value; }
        }

        public abstract void ImportItemsFromSourceControl(DateTime? from, DateTime? to);

        public abstract void ImportUsers(DateTime startDate, DateTime endDate);

        #region IDisposable Members

        public abstract void Dispose();

        #endregion
    }
}
