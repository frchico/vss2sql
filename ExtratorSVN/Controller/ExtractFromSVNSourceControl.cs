using System;
using System.Collections.Generic;
using System.Text;
using SharpSvn;
using System.Data;

using br.com.franciscorodrigues.mestrado.ETL.Model;
using System.IO;
using System.Transactions;
using EntityFramework.BulkInsert.Extensions;
using System.Net;
namespace br.com.franciscorodrigues.mestrado.ETL.Controller
{
    public class ExtractFromSVNSourceControl : ExtractSourceControl
    {
        public ExtractFromSVNSourceControl(string rootPath,  string targetPath, string user, string password)
        {
            base.RootPath = rootPath;
            base.TargetPath = targetPath;
            base.User = user;
            base.Password = password;
        }
        SvnTarget repository;

        SvnClient client = new SvnClient();
        SvnInfoEventArgs info;
        public string CurrentLocalPath
        {
            get { return info.Uri.LocalPath; }
        }
        public bool Open()
        {
            ReportProgress("Loading credentials...");
            if (!string.IsNullOrEmpty(User))
            {
                NetworkCredential cred = new NetworkCredential(User, Password);
                client.Authentication.DefaultCredentials = cred;
            }
            else
            {
                client.Authentication.DefaultCredentials = new NetworkCredential("anonymous", "janeDoe@contoso.com");
            }
            if (SvnTarget.TryParse(RootLocation, out repository) == true)
            {
                try
                {
                    ReportProgress("Loading SVN's info...");
                    client.GetInfo(repository, out info);
                }
                catch (Exception ex)
                {
                    LogUtil.Logger.Debug(ex.Message);
                    throw;
                }
            }
            return true;
        }
        public override void ImportItemsFromSourceControl(DateTime? from, DateTime? to)
        {
            var ETL = new ETLItem
                        {
                            StartDate = from.Value,
                            EndDate = to.Value,
                            Repository = base.RootPath,
                            TargetPath = base.TargetPath,
                            Status = StatusETL.Running,
                            User = base.User
                        };
            using (var contexto = new Context())
            {
                contexto.ETL.Add(ETL);
                contexto.SaveChanges();
                contexto.Configuration.ValidateOnSaveEnabled = false;
                using (var t = contexto.Database.BeginTransaction())
                {
                    try
                    {

                        if (Open())
                        {
                            System.Collections.ObjectModel.Collection<SvnLogEventArgs> logitems = new System.Collections.ObjectModel.Collection<SvnLogEventArgs>();

                            SvnRevision revisonStart = new SvnRevision(ETL.StartDate);
                            SvnRevision revisonEnd = new SvnRevision(ETL.EndDate);
                            SvnLogArgs logargs = new SvnLogArgs(new SvnRevisionRange(revisonStart, revisonEnd));
                            Uri root = info.RepositoryRoot;


                            //TODO: Jogar numa thread
                            //var VSSLogEvents = new List<VSSLogEvent>();
                            //long i = 0;
                            ReportProgress("Recovering commits between {0} and {1}...", from.Value, to.Value);
                            if (client.GetLog(info.Uri, logargs, out logitems))
                            {
                                foreach (SvnLogEventArgs logitem in logitems)
                                {
                                    StringBuilder ChangedPaths = new StringBuilder();

                                    if (logitem.ChangedPaths != null)
                                    {
                                        foreach (SvnChangeItem path in logitem.ChangedPaths)
                                        {
                                            var item = SvnChangeItemToVSSLogEvents(ETL, root, logitem, ChangedPaths, path);
                                            if (item != null)
                                                ETL.VSSLogEvents.Add(item);
                                        }
                                    }

                                }
                            }
                        }
                        ETL.Status = StatusETL.Completed;
                        contexto.ReloadNavigationProperty<ETLItem, VSSLogEvent>(ETL, e => e.VSSLogEvents);
                        contexto.SaveChanges();
                        t.Commit();
                        ReportProgress("Process completed with success.");
                    }
                    catch (Exception erro)
                    {
                        try
                        {
                            ETL.Status = StatusETL.Error;
                            contexto.SaveChanges();
                            t.Commit();
                        }
                        catch
                        {
                            throw erro;
                        }
                    }
                    finally
                    {
                        //client.Dispose();
                    }
                }
            }
        }

        private VSSLogEvent SvnChangeItemToVSSLogEvents(ETLItem ETL, Uri root, SvnLogEventArgs logitem, StringBuilder ChangedPaths, SvnChangeItem path)
        {
            VSSLogEvent VSSLogEvent = null;
            string currentDir = info.Uri.OriginalString.Remove(0, root.OriginalString.Length - 1);
            string currentDirUnescaped = Uri.UnescapeDataString(currentDir);
            //TODO: Verificar se tem outra forma de obter os itens de um subdiretorio
            if (!path.Path.StartsWith(currentDir) && !path.Path.StartsWith(currentDirUnescaped))
                return VSSLogEvent;
            string spec = root.AbsoluteUri;
            if (spec[spec.Length - 1] == '/')
            {
                spec = spec.Substring(0, spec.Length - 1);
            }
            spec = spec + path.Path;
            ReportProgress("Loading " + spec + "...");
            string file = null;
            int? LOC = null;
            byte[] fileContent = null;
            bool isFile = !string.IsNullOrEmpty(Path.GetExtension(spec));
            if (isFile)
            {
                ///TODO: Melhorar a forma de obter os itens
                ///<see cref="http://sharpsvntips.net/post/45302481512/retrieving-multiple-versions-of-a-file"
                file = GetFile(path, spec, logitem.Revision);
            }
            else
            {
                file = "";
            }
            try
            {
                var name = Path.GetFileName(path.Path);
                if (isFile && !string.IsNullOrEmpty(file) && File.Exists(file))
                {

                    fileContent = File.ReadAllBytes(file);
                    LOC = File.ReadAllLines(file).Length;
                }
                VSSLogEvent = new VSSLogEvent
                {
                    Deleted = path.Action == SvnChangeAction.Delete,
                    Name = name,
                    Spec = spec,
                    Type = isFile ? VSSLogEvent.ItemType.File : VSSLogEvent.ItemType.Folder,
                    VersionNumber = logitem.Revision.ToString(),
                    Author = logitem.Author,
                    DateCheckIn = logitem.Time,
                    DateLastModification = logitem.Time,
                    Comment = logitem.LogMessage,
                    FileContent = fileContent,
                    Action = path.Action.ToString(),
                    Lines = LOC,
                    ETLId = ETL.Id
                };
            }
            finally
            {
                string msg = string.Format("Deleting file {0}", file);
                if (File.Exists(file))
                {
                    File.Delete(file);
                    ReportProgress("{0} - OK", msg);
                }
                else
                    ReportProgress("{0} - Arquivo n�o encontrado.", msg);

            }
            ChangedPaths.AppendFormat("{1} {2}{0}", Environment.NewLine, path.Action, path.Path);
            if (path.CopyFromRevision != -1)
            {
                ChangedPaths.AppendFormat("{1} -> {2}{0}", Environment.NewLine, path.CopyFromPath, path.CopyFromRevision);
            }
            return VSSLogEvent;
        }

        /// <summary>
        /// TODO: Mudar para <see cref="http://sharpsvntips.net/post/45301798139/getting-file-content-from-repository-in-memory"/>
        /// </summary>
        /// <param name="path"></param>
        /// <param name="svnFilePath"></param>
        /// <param name="revision"></param>
        /// <returns></returns>
        private string GetFile(SvnChangeItem path, string svnFilePath, long revision)
        {
            string output = Path.Combine(base.TempPath, System.IO.Path.GetFileName(path.Path));
            ReportProgress("Get file \"{0}\", revision {1} to {2}", svnFilePath, revision, output);
            SvnExportArgs sea = new SvnExportArgs();
            sea.Revision = revision;
            
            
            SvnUpdateResult sur;
            try
            {
                if (client.Export(svnFilePath, output, sea, out sur))
                    return output;
            }
            catch( Exception e)
            {
                LogUtil.Logger.Error(e);
            }
            return null;

        }

        public override void ImportUsers(DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public override void Dispose()
        {
            ReportProgress("Disposing ExtractFromSVNSourceControl...");
            if (this.client != null)
            {
                client.Dispose();
            }
            ReportProgress("Dispose OK!");
            
        }
    }
}