﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace br.com.franciscorodrigues.mestrado.ETL.Model
{
    public class Configuration
    {
        [Key]
        public long Id { get; set; }
        [StringLength(50),
        Required]
        public String Nome { get; set; }
        [StringLength(1000),
        Required]
        public String RootPath { get; set; }
        [StringLength(500),
        Required]
        public String TargetPath { get; set; }
        [StringLength(500)]
        public String UserName { get; set; }
        [StringLength(400)]
        public String Pwd { get; set; }
        [StringLength(500),
        Required]
        public String TempPath { get; set; }
        [Required]
        public bool ApplyCleaning { get; set; }
        public bool DeleteAllLoadHistory { get; set; }
        [Required, StringLength(10)]
        public String RepoType { get; set; }
        public Configuration()
        {
            ApplyCleaning = true;
            DeleteAllLoadHistory = true;
            RepoType = "SVN";
            TargetPath = "/";
            this.TempPath = System.IO.Path.GetTempPath();            
        }
    }
}
