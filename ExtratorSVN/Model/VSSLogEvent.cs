﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace br.com.franciscorodrigues.mestrado.ETL.Model
{
    [Table("VSSLogEvent")]
    public class VSSLogEvent
    {
        public enum ItemType : int{
            Folder =0,
            File = 1
        }
        public VSSLogEvent ()
	    {
            Deleted = false;
        }
        [Key]
        public long Id { get; set; }

        public long ETLId { get; set; }
        public ETLItem ETL { get; set; }


        /// <summary>
        /// Informa se foi apagado o item
        /// </summary>
        [Required]        
        public bool Deleted { get; set; }
        /// <summary>
        /// Informa se está bloqueado pelo usuário para alteração
        /// </summary>
        public int? IsCheckedOut { get; set; }
        /// <summary>
        /// Local que o usuário fez o checkout
        /// </summary>
        [StringLength(2000)]
        public String LocalCheckOut { get; set; }
        /// <summary>
        /// Nome do arquivo/pasta
        /// </summary>
	    public String Name { get; set; }
        /// <summary>
        /// Subdiretório onde está associado o item
        /// </summary>
        public long? ParentId { get; set; }
        /// <summary>
        /// Path no source safe do item
        /// </summary>
	    [StringLength(2000)]
        public String Spec { get; set; }
        /// <summary>
        /// Tipo do item...(pasta / arquivo )
        /// </summary>
        public ItemType Type { get; set; }
        /// <summary>
        /// Identificador da versão
        /// </summary>
        [Required]
        public String VersionNumber { get; set; }
        /// <summary>
        /// Autor
        /// </summary>
        [StringLength(200)]
	    public String Author { get; set; }
        /// <summary>
        /// DateCheckIn
        /// </summary>
	    public DateTime? DateCheckIn { get; set; }
        /// <summary>
        /// DateLastModification
        /// </summary>
        public DateTime? DateLastModification { get; set; }
        /// <summary>
        /// Label para a versão
        /// </summary>
	    [StringLength(50)]
        public String Label { get; set; }
        /// <summary>
        /// Comentário do commit
        /// </summary>
        [StringLength(4000)]
	    public String Comment { get; set; }
        /// <summary>
        /// Arquivo
        /// </summary>
        [Column(TypeName = "image")]
        public byte[] FileContent { get; set; }
        [StringLength(2000)]
	    public String Action { get; set; }
	    public int? Lines { get; set; }
	    
    }
}
