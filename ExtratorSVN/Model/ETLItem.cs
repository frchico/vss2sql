﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace br.com.franciscorodrigues.mestrado.ETL.Model
{
    [Table("ETL")]
    public class ETLItem
    {
        public ETLItem()
        {
            Date = DateTime.Now;
            Status = StatusETL.Running;
            this.VSSLogEvents = new List<VSSLogEvent>();
        }
        [Key]
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public String Repository { get; set; }
        public String TargetPath { get; set; }
        public String User { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public StatusETL Status { get; set; }

        public virtual IList<VSSLogEvent> VSSLogEvents { get; set; }
    }
}
