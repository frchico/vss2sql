﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace br.com.franciscorodrigues.mestrado.ETL.Model
{
    public enum StatusETL
    {
        NotStarted,
        Completed,
        Running,
        Error
        
    }
}
