﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace br.com.franciscorodrigues.mestrado.ETL.Model
{
    public class Context: DbContext
    {
        public Context()
            : base("Conexao")
        {

        }
        public DbSet<ETLItem> ETL { get; set; }
        public DbSet<VSSLogEvent> VSSLogEvent { get; set; }
        public DbSet<Configuration> ConfigurationTable { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);            
        }

        public DateTime? ObterProximaDataImportacao(String RootPath = null)
        {
            DateTime? ultimaImportacao = null;
            var count = ETL.Count();
            if (ETL.Count(p=> p.Repository.Equals(RootPath) ) > 0)
            {
                var x =
                    from etl in this.ETL
                    where etl.Repository.Equals(RootPath) 
                    select (etl.EndDate);                    
                var y = x.ToList();
                ultimaImportacao = y.Max();
                if (ultimaImportacao.HasValue)
                {
                    ultimaImportacao = ultimaImportacao.Value.AddSeconds(1);
                }
            }
            return ultimaImportacao;
        }
    }
}
