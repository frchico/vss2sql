﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using br.com.franciscorodrigues.mestrado.ETL.View;
using System.Configuration;
namespace br.com.franciscorodrigues.mestrado.ETL
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( params String[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (ConfigurationManager.ConnectionStrings.Count == 0 )
            {
                Application.Run(new SettingDataBase());
            }
            else
            {
                Application.Run(new ListConfigurationForm());
            }
            
        }
    }
}
