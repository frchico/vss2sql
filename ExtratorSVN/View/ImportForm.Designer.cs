﻿namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    partial class ImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxGeral = new System.Windows.Forms.GroupBox();
            this.chkParseFiles = new System.Windows.Forms.CheckBox();
            this.lblConfiguration = new System.Windows.Forms.Label();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnExecutar = new System.Windows.Forms.Button();
            this.lblTo = new System.Windows.Forms.Label();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.dtpBegin = new System.Windows.Forms.DateTimePicker();
            this.lblMessage = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.gbxGeral.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxGeral
            // 
            this.gbxGeral.Controls.Add(this.chkParseFiles);
            this.gbxGeral.Controls.Add(this.lblConfiguration);
            this.gbxGeral.Controls.Add(this.cmbTipo);
            this.gbxGeral.Controls.Add(this.btnSair);
            this.gbxGeral.Controls.Add(this.btnExecutar);
            this.gbxGeral.Controls.Add(this.lblTo);
            this.gbxGeral.Controls.Add(this.dtpEnd);
            this.gbxGeral.Controls.Add(this.lblFrom);
            this.gbxGeral.Controls.Add(this.dtpBegin);
            this.gbxGeral.Controls.Add(this.lblMessage);
            this.gbxGeral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxGeral.Location = new System.Drawing.Point(0, 0);
            this.gbxGeral.Name = "gbxGeral";
            this.gbxGeral.Size = new System.Drawing.Size(380, 311);
            this.gbxGeral.TabIndex = 4;
            this.gbxGeral.TabStop = false;
            // 
            // chkParseFiles
            // 
            this.chkParseFiles.AutoSize = true;
            this.chkParseFiles.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.chkParseFiles.Enabled = false;
            this.chkParseFiles.Location = new System.Drawing.Point(21, 98);
            this.chkParseFiles.Name = "chkParseFiles";
            this.chkParseFiles.Size = new System.Drawing.Size(77, 17);
            this.chkParseFiles.TabIndex = 10;
            this.chkParseFiles.Text = "Parse Files";
            this.chkParseFiles.UseVisualStyleBackColor = true;
            // 
            // lblConfiguration
            // 
            this.lblConfiguration.AutoSize = true;
            this.lblConfiguration.Location = new System.Drawing.Point(6, 74);
            this.lblConfiguration.Name = "lblConfiguration";
            this.lblConfiguration.Size = new System.Drawing.Size(72, 13);
            this.lblConfiguration.TabIndex = 7;
            this.lblConfiguration.Text = "Configuration:";
            // 
            // cmbTipo
            // 
            this.cmbTipo.DisplayMember = "configurationName";
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Location = new System.Drawing.Point(84, 71);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(246, 21);
            this.cmbTipo.Sorted = true;
            this.cmbTipo.TabIndex = 2;
            this.cmbTipo.SelectedIndexChanged += new System.EventHandler(this.cmbTipo_SelectedIndexChanged);
            this.cmbTipo.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cmbTipo_Format);
            // 
            // btnSair
            // 
            this.btnSair.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnSair.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.Sair;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnSair.Location = new System.Drawing.Point(203, 141);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(81, 23);
            this.btnSair.TabIndex = 4;
            this.btnSair.Text = "&Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnExecutar
            // 
            this.btnExecutar.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.Executar;
            this.btnExecutar.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnExecutar.Location = new System.Drawing.Point(116, 141);
            this.btnExecutar.Name = "btnExecutar";
            this.btnExecutar.Size = new System.Drawing.Size(81, 23);
            this.btnExecutar.TabIndex = 3;
            this.btnExecutar.Text = "&Executar";
            this.btnExecutar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExecutar.UseVisualStyleBackColor = true;
            this.btnExecutar.Click += new System.EventHandler(this.btnExecutar_Click);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(55, 49);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 13);
            this.lblTo.TabIndex = 3;
            this.lblTo.Text = "To:";
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(84, 45);
            this.dtpEnd.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.ShowCheckBox = true;
            this.dtpEnd.Size = new System.Drawing.Size(246, 20);
            this.dtpEnd.TabIndex = 1;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(45, 23);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(33, 13);
            this.lblFrom.TabIndex = 1;
            this.lblFrom.Text = "From:";
            // 
            // dtpBegin
            // 
            this.dtpBegin.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpBegin.Location = new System.Drawing.Point(84, 19);
            this.dtpBegin.MinDate = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.dtpBegin.Name = "dtpBegin";
            this.dtpBegin.ShowCheckBox = true;
            this.dtpBegin.Size = new System.Drawing.Size(246, 20);
            this.dtpBegin.TabIndex = 0;
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.SystemColors.Control;
            this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblMessage.Location = new System.Drawing.Point(3, 191);
            this.lblMessage.Multiline = true;
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(374, 117);
            this.lblMessage.TabIndex = 9;
            this.lblMessage.Text = "Progress...";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssMsg});
            this.statusStrip1.Location = new System.Drawing.Point(0, 311);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(380, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssMsg
            // 
            this.tssMsg.Name = "tssMsg";
            this.tssMsg.Size = new System.Drawing.Size(0, 17);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // ImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 333);
            this.Controls.Add(this.gbxGeral);
            this.Controls.Add(this.statusStrip1);
            this.MaximumSize = new System.Drawing.Size(396, 372);
            this.MinimumSize = new System.Drawing.Size(396, 372);
            this.Name = "ImportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImportForm_FormClosing);
            this.Load += new System.EventHandler(this.ExtracaoForm_Load);
            this.gbxGeral.ResumeLayout(false);
            this.gbxGeral.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxGeral;
        private System.Windows.Forms.CheckBox chkParseFiles;
        private System.Windows.Forms.Label lblConfiguration;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnExecutar;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.DateTimePicker dtpBegin;
        private System.Windows.Forms.TextBox lblMessage;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssMsg;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}