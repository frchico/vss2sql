using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ManagementExtractionConsole
{
    public class FileToView: IComparable
    {
        string filePath;

        
        public FileToView(string FilePath)
        {
            this.FilePath = FilePath;
        }

        public string FileName
        {
            get
            {
                if (FilePath == null) return null;

                return Path.GetFileName(FilePath);
            }
        }
        public string FilePath
        {
            get { return filePath; }
            private set { filePath = value; }
        }
        public override string ToString()
        {
            return FileName;
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj is FileToView)
            {
                FileToView f = (FileToView)obj;
                return this.FileName.CompareTo(f.FileName);
            }
            return 0;
        }

        #endregion

    }
}
