﻿using br.com.franciscorodrigues.mestrado.ETL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    public partial class ListConfigurationForm : Form
    {
        public ListConfigurationForm()
        {
            InitializeComponent();
        }

        Context contexto = new Context();
        private void btnNew_Click(object sender, EventArgs e)
        {
            new ConfigurationForm().ShowDialog();
            this.RefreshData();
        }

        private void showDetails(bool canEdit)
        {
            if (dgvList.SelectedRows.Count == 0)
            {
                return;
            }
            ConfigurationForm config = new ConfigurationForm(canEdit);
            config.Tag = ((Configuration)dgvList.SelectedRows[0].DataBoundItem).Id;
            config.ShowDialog();
            this.RefreshData();
        }
        private void btnView_Click(object sender, EventArgs e)
        {
            showDetails(false);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            showDetails(true);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var item = dgvList.SelectedRows[0].DataBoundItem as Configuration;
            var config = contexto.ConfigurationTable.Find(item.Id);
            contexto.ConfigurationTable.Remove(config);
            contexto.SaveChanges();               
            this.RefreshData();
        }

        private void RefreshData()
        {

            contexto = new Context();
            dgvList.DataSource = contexto.ConfigurationTable.ToList();
        }

        private void ListConfigurationForm_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void toolStripButtonImportar_Click(object sender, EventArgs e)
        {
            new ImportForm().ShowDialog();
        }

        private void configuraçãoDoBancoDeDaosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SettingDataBase().ShowDialog();

        }
    }
}
