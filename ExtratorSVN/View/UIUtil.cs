﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Forms;

namespace br.com.franciscorodrigues.mestrado.ETL
{
    static class UIUtil
    {
        public static void ReloadEntity<TEntity>(this DbContext context, TEntity entity) 
            where TEntity : class
        {
            context.Entry(entity).Reload();
        }
        public static void ReloadNavigationProperty<TEntity, TElement>(this DbContext context, TEntity entity, 
            Expression<Func<TEntity, ICollection<TElement>>> navigationProperty) 
            where TEntity : class 
            where TElement : class
        {
            context.Entry(entity).Collection<TElement>(navigationProperty).Query();
        }
        public static Boolean wasFilled(Control control, ErrorProvider errorProvider)
        {
            Boolean comErro = false;
            ValideNulo(control, errorProvider, ref comErro);
            return !comErro;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <param name="errorProvider"></param>
        /// <param name="hasError"></param>
        /// <returns>False when found error</returns>
        public static void ValideNulo(Control control, ErrorProvider errorProvider, ref bool hasError)
        {
            if (
                    (control is TextBox && string.IsNullOrEmpty(control.Text)) ||
                    (control is ComboBox && ((ComboBox)control).SelectedItem == null)
                )
            {
                errorProvider.SetError(control, "necessary.");
                hasError |= true;
            }
        }
    }
}
