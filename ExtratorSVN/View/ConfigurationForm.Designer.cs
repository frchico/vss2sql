﻿namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    partial class ConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbxApagarPeriodoBase = new System.Windows.Forms.CheckBox();
            this.tbxConfigurationName = new System.Windows.Forms.TextBox();
            this.lblConfigurationName = new System.Windows.Forms.Label();
            this.gbxType = new System.Windows.Forms.GroupBox();
            this.rbtSVN = new System.Windows.Forms.RadioButton();
            this.rbtVSS = new System.Windows.Forms.RadioButton();
            this.btnTempPath = new System.Windows.Forms.Button();
            this.tbxTempPath = new System.Windows.Forms.TextBox();
            this.tbxTargetPath = new System.Windows.Forms.TextBox();
            this.lblSpec = new System.Windows.Forms.Label();
            this.lblTempPath = new System.Windows.Forms.Label();
            this.btnLocalizar = new System.Windows.Forms.Button();
            this.lblSrcSafeIni = new System.Windows.Forms.Label();
            this.tbxPassword = new System.Windows.Forms.TextBox();
            this.cbxApplyCleaning = new System.Windows.Forms.CheckBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tbxRootPath = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.lblSenha = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tbxUser = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.gbxOutros = new System.Windows.Forms.GroupBox();
            this.gbxVSS = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.gbxType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.gbxOutros.SuspendLayout();
            this.gbxVSS.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxApagarPeriodoBase
            // 
            this.cbxApagarPeriodoBase.AutoSize = true;
            this.cbxApagarPeriodoBase.Location = new System.Drawing.Point(136, 45);
            this.cbxApagarPeriodoBase.Name = "cbxApagarPeriodoBase";
            this.cbxApagarPeriodoBase.Size = new System.Drawing.Size(98, 17);
            this.cbxApagarPeriodoBase.TabIndex = 4;
            this.cbxApagarPeriodoBase.Text = "Delete all loads";
            this.cbxApagarPeriodoBase.UseVisualStyleBackColor = true;
            // 
            // tbxConfigurationName
            // 
            this.tbxConfigurationName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxConfigurationName.Location = new System.Drawing.Point(80, 25);
            this.tbxConfigurationName.Name = "tbxConfigurationName";
            this.tbxConfigurationName.Size = new System.Drawing.Size(478, 20);
            this.tbxConfigurationName.TabIndex = 2;
            // 
            // lblConfigurationName
            // 
            this.lblConfigurationName.AutoSize = true;
            this.lblConfigurationName.Location = new System.Drawing.Point(42, 28);
            this.lblConfigurationName.Name = "lblConfigurationName";
            this.lblConfigurationName.Size = new System.Drawing.Size(38, 13);
            this.lblConfigurationName.TabIndex = 1;
            this.lblConfigurationName.Text = "Name:";
            // 
            // gbxType
            // 
            this.gbxType.Controls.Add(this.rbtSVN);
            this.gbxType.Controls.Add(this.rbtVSS);
            this.gbxType.Location = new System.Drawing.Point(12, 51);
            this.gbxType.Name = "gbxType";
            this.gbxType.Size = new System.Drawing.Size(533, 50);
            this.gbxType.TabIndex = 3;
            this.gbxType.TabStop = false;
            this.gbxType.Text = "Type:";
            // 
            // rbtSVN
            // 
            this.rbtSVN.AutoSize = true;
            this.rbtSVN.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtSVN.Location = new System.Drawing.Point(49, 16);
            this.rbtSVN.Name = "rbtSVN";
            this.rbtSVN.Size = new System.Drawing.Size(47, 31);
            this.rbtSVN.TabIndex = 1;
            this.rbtSVN.Text = "SVN";
            this.rbtSVN.UseVisualStyleBackColor = true;
            // 
            // rbtVSS
            // 
            this.rbtVSS.AutoSize = true;
            this.rbtVSS.Checked = true;
            this.rbtVSS.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtVSS.Location = new System.Drawing.Point(3, 16);
            this.rbtVSS.Name = "rbtVSS";
            this.rbtVSS.Size = new System.Drawing.Size(46, 31);
            this.rbtVSS.TabIndex = 0;
            this.rbtVSS.TabStop = true;
            this.rbtVSS.Text = "VSS";
            this.rbtVSS.UseVisualStyleBackColor = true;
            // 
            // btnTempPath
            // 
            this.btnTempPath.Location = new System.Drawing.Point(428, 18);
            this.btnTempPath.Name = "btnTempPath";
            this.btnTempPath.Size = new System.Drawing.Size(24, 23);
            this.btnTempPath.TabIndex = 2;
            this.btnTempPath.UseVisualStyleBackColor = true;
            this.btnTempPath.Click += new System.EventHandler(this.btnTempPath_Click);
            // 
            // tbxTempPath
            // 
            this.tbxTempPath.Location = new System.Drawing.Point(118, 19);
            this.tbxTempPath.Name = "tbxTempPath";
            this.tbxTempPath.Size = new System.Drawing.Size(303, 20);
            this.tbxTempPath.TabIndex = 1;
            // 
            // tbxTargetPath
            // 
            this.tbxTargetPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxTargetPath.Location = new System.Drawing.Point(80, 183);
            this.tbxTargetPath.Name = "tbxTargetPath";
            this.tbxTargetPath.Size = new System.Drawing.Size(478, 20);
            this.tbxTargetPath.TabIndex = 0;
            // 
            // lblSpec
            // 
            this.lblSpec.AutoSize = true;
            this.lblSpec.Location = new System.Drawing.Point(9, 186);
            this.lblSpec.Name = "lblSpec";
            this.lblSpec.Size = new System.Drawing.Size(65, 13);
            this.lblSpec.TabIndex = 11;
            this.lblSpec.Text = "Target path:";
            // 
            // lblTempPath
            // 
            this.lblTempPath.AutoSize = true;
            this.lblTempPath.Location = new System.Drawing.Point(26, 23);
            this.lblTempPath.Name = "lblTempPath";
            this.lblTempPath.Size = new System.Drawing.Size(89, 13);
            this.lblTempPath.TabIndex = 0;
            this.lblTempPath.Text = "Pasta temporária:";
            // 
            // btnLocalizar
            // 
            this.btnLocalizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLocalizar.Location = new System.Drawing.Point(571, 157);
            this.btnLocalizar.Name = "btnLocalizar";
            this.btnLocalizar.Size = new System.Drawing.Size(24, 23);
            this.btnLocalizar.TabIndex = 10;
            this.btnLocalizar.Text = "...";
            this.btnLocalizar.UseVisualStyleBackColor = true;
            this.btnLocalizar.Click += new System.EventHandler(this.btnLocalizar_Click);
            // 
            // lblSrcSafeIni
            // 
            this.lblSrcSafeIni.AutoSize = true;
            this.lblSrcSafeIni.Location = new System.Drawing.Point(17, 162);
            this.lblSrcSafeIni.Name = "lblSrcSafeIni";
            this.lblSrcSafeIni.Size = new System.Drawing.Size(57, 13);
            this.lblSrcSafeIni.TabIndex = 8;
            this.lblSrcSafeIni.Text = "Root path:";
            // 
            // tbxPassword
            // 
            this.tbxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxPassword.Location = new System.Drawing.Point(80, 133);
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.Size = new System.Drawing.Size(478, 20);
            this.tbxPassword.TabIndex = 7;
            // 
            // cbxApplyCleaning
            // 
            this.cbxApplyCleaning.AutoSize = true;
            this.cbxApplyCleaning.Location = new System.Drawing.Point(26, 45);
            this.cbxApplyCleaning.Name = "cbxApplyCleaning";
            this.cbxApplyCleaning.Size = new System.Drawing.Size(96, 17);
            this.cbxApplyCleaning.TabIndex = 3;
            this.cbxApplyCleaning.Text = "Apply Cleaning";
            this.cbxApplyCleaning.UseVisualStyleBackColor = true;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // tbxRootPath
            // 
            this.tbxRootPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxRootPath.Location = new System.Drawing.Point(80, 159);
            this.tbxRootPath.Name = "tbxRootPath";
            this.tbxRootPath.Size = new System.Drawing.Size(478, 20);
            this.tbxRootPath.TabIndex = 9;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(170, 326);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(90, 23);
            this.btnSalvar.TabIndex = 6;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.tsbSalvar_Click);
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.Location = new System.Drawing.Point(18, 136);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(56, 13);
            this.lblSenha.TabIndex = 6;
            this.lblSenha.Text = "Password:";
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "*.ini|*.ini";
            this.openFileDialog.FileName = "srcsafe.ini";
            this.openFileDialog.Filter = "SourceSafe DataBases(srcsafe.ini)|srcsafe.ini";
            // 
            // tbxUser
            // 
            this.tbxUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxUser.Location = new System.Drawing.Point(80, 107);
            this.tbxUser.Name = "tbxUser";
            this.tbxUser.Size = new System.Drawing.Size(478, 20);
            this.tbxUser.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "User:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(270, 326);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(90, 23);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.tsbCancelar_Click);
            // 
            // gbxOutros
            // 
            this.gbxOutros.Controls.Add(this.cbxApagarPeriodoBase);
            this.gbxOutros.Controls.Add(this.cbxApplyCleaning);
            this.gbxOutros.Controls.Add(this.btnTempPath);
            this.gbxOutros.Controls.Add(this.tbxTempPath);
            this.gbxOutros.Controls.Add(this.lblTempPath);
            this.gbxOutros.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbxOutros.Location = new System.Drawing.Point(0, 218);
            this.gbxOutros.Name = "gbxOutros";
            this.gbxOutros.Size = new System.Drawing.Size(609, 82);
            this.gbxOutros.TabIndex = 5;
            this.gbxOutros.TabStop = false;
            this.gbxOutros.Text = "Outros:";
            // 
            // gbxVSS
            // 
            this.gbxVSS.Controls.Add(this.tbxConfigurationName);
            this.gbxVSS.Controls.Add(this.lblConfigurationName);
            this.gbxVSS.Controls.Add(this.gbxType);
            this.gbxVSS.Controls.Add(this.tbxTargetPath);
            this.gbxVSS.Controls.Add(this.lblSpec);
            this.gbxVSS.Controls.Add(this.btnLocalizar);
            this.gbxVSS.Controls.Add(this.tbxRootPath);
            this.gbxVSS.Controls.Add(this.lblSrcSafeIni);
            this.gbxVSS.Controls.Add(this.tbxPassword);
            this.gbxVSS.Controls.Add(this.lblSenha);
            this.gbxVSS.Controls.Add(this.tbxUser);
            this.gbxVSS.Controls.Add(this.label1);
            this.gbxVSS.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbxVSS.Location = new System.Drawing.Point(0, 0);
            this.gbxVSS.Name = "gbxVSS";
            this.gbxVSS.Size = new System.Drawing.Size(609, 218);
            this.gbxVSS.TabIndex = 0;
            this.gbxVSS.TabStop = false;
            this.gbxVSS.Text = "Source Control";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(376, 326);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Importar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 367);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.gbxOutros);
            this.Controls.Add(this.gbxVSS);
            this.Name = "ConfigurationForm";
            this.Text = "ConfigurationForm";
            this.Load += new System.EventHandler(this.Configuration_Load);
            this.gbxType.ResumeLayout(false);
            this.gbxType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.gbxOutros.ResumeLayout(false);
            this.gbxOutros.PerformLayout();
            this.gbxVSS.ResumeLayout(false);
            this.gbxVSS.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox cbxApagarPeriodoBase;
        private System.Windows.Forms.TextBox tbxConfigurationName;
        private System.Windows.Forms.Label lblConfigurationName;
        private System.Windows.Forms.GroupBox gbxType;
        private System.Windows.Forms.RadioButton rbtSVN;
        private System.Windows.Forms.RadioButton rbtVSS;
        private System.Windows.Forms.Button btnTempPath;
        private System.Windows.Forms.TextBox tbxTempPath;
        private System.Windows.Forms.TextBox tbxTargetPath;
        private System.Windows.Forms.Label lblSpec;
        private System.Windows.Forms.Label lblTempPath;
        private System.Windows.Forms.Button btnLocalizar;
        private System.Windows.Forms.Label lblSrcSafeIni;
        private System.Windows.Forms.TextBox tbxPassword;
        private System.Windows.Forms.CheckBox cbxApplyCleaning;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox gbxOutros;
        private System.Windows.Forms.GroupBox gbxVSS;
        private System.Windows.Forms.TextBox tbxRootPath;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.TextBox tbxUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button button1;
    }
}