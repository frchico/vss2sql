﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    public partial class SettingDataBase : Form
    {
        public String Diretorio { get; set; }
        public SettingDataBase()
        {
            InitializeComponent();
            this.Diretorio = Environment.CurrentDirectory;
            folderBrowserDialog.SelectedPath = this.Diretorio;
        }

        private void SettingDataBase_Load(object sender, EventArgs e)
        {
            var conexao = ConfigurationManager.ConnectionStrings["Conexao"];
            if (conexao != null)
            {
                try
                {
                    if (conexao.ProviderName == "System.Data.SqlClient")
                    {
                        var strCon = conexao.ConnectionString.Split(new char[]{';'}, StringSplitOptions.RemoveEmptyEntries).ToList();
                        var strProp = new System.Collections.Specialized.HybridDictionary(strCon.Count, false);
                        strCon.ForEach(delegate(string s)
                        {
                            var item = s.Split('=');
                            strProp.Add(item[0], item.Length > 1 ? item[1] : "");
                        });
                        tbxServer.Text = strProp["Data Source"].ToString();
                        tbxDataBase.Text = strProp["Initial Catalog"].ToString();
                        tbxUser.Text = strProp["User ID"].ToString();
                        tbxPassword.Text = strProp["Password"].ToString();
                    }
                    else
                    {
                        rbtServer.Checked = false;
                        var strCon = conexao.ConnectionString.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                        tbxServer.Text = String.Join("\\", strCon, 0, strCon.Length - 1);
                        tbxServer.Text = tbxServer.Text.Split('=')[1];
                        tbxDataBase.Text = Path.GetFileNameWithoutExtension(strCon[strCon.Length - 1]);

                    }
                    rbtLocal.Checked = !rbtServer.Checked;

                }
                catch (Exception erro)
                {
                    MessageBox.Show("Não foi possível carregar as configurações da aplicação.");
                }
            }
        }

        private void rbtSVN_CheckedChanged(object sender, EventArgs e)
        {
            lblDataBase.Text = "DataBase:";
            lblServer.Text = "Server:";
            bool SqlCE = rbtLocal.Checked;
            if (SqlCE)
            {
                lblServer.Text = "Diretório:";
                lblDataBase.Text = "Arquivo:";
                lblDataBase.Refresh();
                
            }
            btnFolder.Visible = SqlCE;
            lblSenha.Visible = !SqlCE;
            lblUser.Visible = lblSenha.Visible;
            tbxPassword.Visible = lblSenha.Visible;
            tbxUser.Visible = lblSenha.Visible;
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            AlterarConfiguracao(Diretorio);
        }
        public void AlterarConfiguracao(string targetDirectory)
        {
            string exePath = Path.Combine(targetDirectory, "VSS2SQL.exe");

            Configuration config = ConfigurationManager.OpenExeConfiguration(exePath);
            if (config.ConnectionStrings.ConnectionStrings.Count == 0)
            {
                config.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings("Conexao",""));
            }
            var conexao = config.ConnectionStrings.ConnectionStrings["Conexao"];
            
            if (rbtLocal.Checked)
            {
                if ( string.IsNullOrWhiteSpace(tbxDataBase.Text))
                {
                    tbxDataBase.Text="|DataDirectory|";
                }
                conexao.ConnectionString = string.Format("Data Source={0}\\{1}.sdf", tbxServer.Text, tbxDataBase.Text);
                conexao.ProviderName = "System.Data.SqlServerCe.4.0";
            }
            else
            {
                conexao.ConnectionString = string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}",
                    tbxServer.Text, tbxDataBase.Text, tbxUser.Text, tbxPassword.Text);
                conexao.ProviderName = "System.Data.SqlClient";
            }
            
            config.Save();
            if (MessageBox.Show("Deseja fazer uso das novas configurações? Atenção: Isso irá reiniciar a aplicação.", "Aplicar as configurações", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Restart();
            }
        }

        

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                this.tbxServer.Text = folderBrowserDialog.SelectedPath;
            }
        }

    }
}
