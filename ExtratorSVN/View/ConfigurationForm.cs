﻿using br.com.franciscorodrigues.mestrado.ETL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    public partial class ConfigurationForm : Form
    {
        public ConfigurationForm()
        {
           InitializeComponent();
        }
        public ConfigurationForm(bool canEdit)
            : this()
        {
            btnSalvar.Enabled = canEdit;
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                tbxRootPath.Text = openFileDialog.FileName;
            }
        }

        private void btnTempPath_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                tbxTempPath.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void ValideNulo(TextBox control)
        {
            if (string.IsNullOrEmpty(control.Text))
            {
                errorProvider.SetError(control, "necessary.");
                hasError = true;
            }

        }
        bool hasError = false;
        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            hasError = false;
            errorProvider.Clear();
            ValideNulo(tbxConfigurationName);
            ValideNulo(tbxRootPath);
            ValideNulo(tbxTempPath);

            if (hasError)
            {
                return;
            }
            
            Contexto.SaveChanges();
            MessageBox.Show("Gravado com sucesso.");
            this.Close();
        }

        Context Contexto = new Context();
        Configuration datasource = null;

        private void Configuration_Load(object sender, EventArgs e)
        {
           // Configuration datasource;

            if (this.Tag != null)
            {
                datasource = Contexto.ConfigurationTable.Find(this.Tag);
            }
            else
            {
                datasource = Contexto.ConfigurationTable.Add(new Configuration());
            }
            BindingSource data = new BindingSource();
            data.DataSource = datasource;
            if (data != null)
            {
                tbxConfigurationName.DataBindings.Add("Text", data, "Nome");
                tbxRootPath.DataBindings.Add("Text", data, "RootPath");
                tbxPassword.DataBindings.Add("Text", data, "Pwd");
                tbxTempPath.DataBindings.Add("Text", data, "TempPath");
                tbxUser.DataBindings.Add("Text", data, "Username");
                tbxTargetPath.DataBindings.Add("Text", data, "TargetPath");
                cbxApplyCleaning.DataBindings.Add("Checked", data, "ApplyCleaning");
                cbxApagarPeriodoBase.DataBindings.Add("Checked", data, "DeleteAllLoadHistory");

                rbtVSS.Checked = true;

                if (datasource.RepoType.ToUpper() == "SVN")
                {
                    rbtSVN.Checked = true;
                }
            }
        }
    

        private void tsbCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var importador = new Controller.ExtractFromSVNSourceControl(datasource.RootPath, datasource.TargetPath, datasource.UserName, datasource.Pwd);
            importador.ImportItemsFromSourceControl(new DateTime(2010, 06, 26), DateTime.Now);
        }
    }
}
