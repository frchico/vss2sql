﻿namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    partial class ListConfigurationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvList = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonVisualizar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonNovo = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonEditar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonApagar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonImportar = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.configuraçãoDoBancoDeDaosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvList
            // 
            this.dgvList.AllowUserToAddRows = false;
            this.dgvList.AllowUserToDeleteRows = false;
            this.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvList.Location = new System.Drawing.Point(0, 25);
            this.dgvList.MultiSelect = false;
            this.dgvList.Name = "dgvList";
            this.dgvList.ReadOnly = true;
            this.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvList.Size = new System.Drawing.Size(580, 294);
            this.dgvList.TabIndex = 5;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonNovo,
            this.toolStripButtonVisualizar,
            this.toolStripButtonEditar,
            this.toolStripButtonApagar,
            this.toolStripSeparator1,
            this.toolStripButtonImportar,
            this.toolStripDropDownButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(580, 25);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonVisualizar
            // 
            this.toolStripButtonVisualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonVisualizar.Name = "toolStripButtonVisualizar";
            this.toolStripButtonVisualizar.Size = new System.Drawing.Size(60, 22);
            this.toolStripButtonVisualizar.Text = "Visualizar";
            this.toolStripButtonVisualizar.Click += new System.EventHandler(this.btnView_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonNovo
            // 
            this.toolStripButtonNovo.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.AdicionarArquivo;
            this.toolStripButtonNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonNovo.Name = "toolStripButtonNovo";
            this.toolStripButtonNovo.Size = new System.Drawing.Size(56, 22);
            this.toolStripButtonNovo.Text = "Novo";
            this.toolStripButtonNovo.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // toolStripButtonEditar
            // 
            this.toolStripButtonEditar.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.Configuracoes;
            this.toolStripButtonEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEditar.Name = "toolStripButtonEditar";
            this.toolStripButtonEditar.Size = new System.Drawing.Size(57, 22);
            this.toolStripButtonEditar.Text = "Editar";
            this.toolStripButtonEditar.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // toolStripButtonApagar
            // 
            this.toolStripButtonApagar.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.RemoverArquivo;
            this.toolStripButtonApagar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonApagar.Name = "toolStripButtonApagar";
            this.toolStripButtonApagar.Size = new System.Drawing.Size(65, 22);
            this.toolStripButtonApagar.Text = "Apagar";
            this.toolStripButtonApagar.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // toolStripButtonImportar
            // 
            this.toolStripButtonImportar.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.Executar;
            this.toolStripButtonImportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonImportar.Name = "toolStripButtonImportar";
            this.toolStripButtonImportar.Size = new System.Drawing.Size(73, 22);
            this.toolStripButtonImportar.Text = "Importar";
            this.toolStripButtonImportar.Click += new System.EventHandler(this.toolStripButtonImportar_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraçãoDoBancoDeDaosToolStripMenuItem});
            this.toolStripDropDownButton1.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.Processar;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
            // 
            // configuraçãoDoBancoDeDaosToolStripMenuItem
            // 
            this.configuraçãoDoBancoDeDaosToolStripMenuItem.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.MontarBase;
            this.configuraçãoDoBancoDeDaosToolStripMenuItem.Name = "configuraçãoDoBancoDeDaosToolStripMenuItem";
            this.configuraçãoDoBancoDeDaosToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.configuraçãoDoBancoDeDaosToolStripMenuItem.Text = "Configuração do Banco de dados";
            this.configuraçãoDoBancoDeDaosToolStripMenuItem.Click += new System.EventHandler(this.configuraçãoDoBancoDeDaosToolStripMenuItem_Click);
            // 
            // ListConfigurationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 319);
            this.Controls.Add(this.dgvList);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ListConfigurationForm";
            this.Text = "SVN 2 SQL";
            this.Load += new System.EventHandler(this.ListConfigurationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvList)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvList;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonNovo;
        private System.Windows.Forms.ToolStripButton toolStripButtonVisualizar;
        private System.Windows.Forms.ToolStripButton toolStripButtonEditar;
        private System.Windows.Forms.ToolStripButton toolStripButtonApagar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButtonImportar;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem configuraçãoDoBancoDeDaosToolStripMenuItem;
    }
}