﻿namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    partial class SettingDataBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxType = new System.Windows.Forms.GroupBox();
            this.rbtLocal = new System.Windows.Forms.RadioButton();
            this.rbtServer = new System.Windows.Forms.RadioButton();
            this.lblDataBase = new System.Windows.Forms.Label();
            this.tbxDataBase = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.tbxPassword = new System.Windows.Forms.TextBox();
            this.lblSenha = new System.Windows.Forms.Label();
            this.tbxUser = new System.Windows.Forms.TextBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.btnContinuar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlServerTxt = new System.Windows.Forms.Panel();
            this.tbxServer = new System.Windows.Forms.TextBox();
            this.btnFolder = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.gbxType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlServerTxt.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxType
            // 
            this.gbxType.Controls.Add(this.rbtLocal);
            this.gbxType.Controls.Add(this.rbtServer);
            this.gbxType.Location = new System.Drawing.Point(12, 12);
            this.gbxType.Name = "gbxType";
            this.gbxType.Size = new System.Drawing.Size(543, 50);
            this.gbxType.TabIndex = 0;
            this.gbxType.TabStop = false;
            this.gbxType.Text = "Tipo do banco de dados";
            // 
            // rbtLocal
            // 
            this.rbtLocal.AutoSize = true;
            this.rbtLocal.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtLocal.Location = new System.Drawing.Point(115, 16);
            this.rbtLocal.Name = "rbtLocal";
            this.rbtLocal.Size = new System.Drawing.Size(98, 31);
            this.rbtLocal.TabIndex = 1;
            this.rbtLocal.Text = "Local (SQL CE)";
            this.rbtLocal.UseVisualStyleBackColor = true;
            this.rbtLocal.CheckedChanged += new System.EventHandler(this.rbtSVN_CheckedChanged);
            // 
            // rbtServer
            // 
            this.rbtServer.AutoSize = true;
            this.rbtServer.Checked = true;
            this.rbtServer.Dock = System.Windows.Forms.DockStyle.Left;
            this.rbtServer.Location = new System.Drawing.Point(3, 16);
            this.rbtServer.Name = "rbtServer";
            this.rbtServer.Size = new System.Drawing.Size(112, 31);
            this.rbtServer.TabIndex = 0;
            this.rbtServer.TabStop = true;
            this.rbtServer.Text = "Rede (SQLServer)";
            this.rbtServer.UseVisualStyleBackColor = true;
            // 
            // lblDataBase
            // 
            this.lblDataBase.AutoSize = true;
            this.lblDataBase.Location = new System.Drawing.Point(18, 106);
            this.lblDataBase.Name = "lblDataBase";
            this.lblDataBase.Size = new System.Drawing.Size(57, 13);
            this.lblDataBase.TabIndex = 3;
            this.lblDataBase.Text = "DataBase:";
            this.lblDataBase.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbxDataBase
            // 
            this.tbxDataBase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.helpProvider1.SetHelpString(this.tbxDataBase, "Nome do banco de dados ou o database no servidor. Exemplo para banco local: VSS2S" +
        "QL");
            this.tbxDataBase.Location = new System.Drawing.Point(79, 104);
            this.tbxDataBase.Name = "tbxDataBase";
            this.helpProvider1.SetShowHelp(this.tbxDataBase, true);
            this.tbxDataBase.Size = new System.Drawing.Size(469, 20);
            this.tbxDataBase.TabIndex = 4;
            this.tbxDataBase.Text = "VSS2SQL";
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(34, 81);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(41, 13);
            this.lblServer.TabIndex = 1;
            this.lblServer.Text = "Server:";
            this.lblServer.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbxPassword
            // 
            this.tbxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxPassword.Location = new System.Drawing.Point(79, 152);
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.Size = new System.Drawing.Size(469, 20);
            this.tbxPassword.TabIndex = 8;
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.Location = new System.Drawing.Point(19, 156);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(56, 13);
            this.lblSenha.TabIndex = 7;
            this.lblSenha.Text = "Password:";
            this.lblSenha.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbxUser
            // 
            this.tbxUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxUser.Location = new System.Drawing.Point(79, 128);
            this.tbxUser.Name = "tbxUser";
            this.tbxUser.Size = new System.Drawing.Size(469, 20);
            this.tbxUser.TabIndex = 6;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(43, 131);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(32, 13);
            this.lblUser.TabIndex = 5;
            this.lblUser.Text = "User:";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnContinuar
            // 
            this.btnContinuar.Location = new System.Drawing.Point(473, 179);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(75, 23);
            this.btnContinuar.TabIndex = 9;
            this.btnContinuar.Text = "Continuar";
            this.btnContinuar.UseVisualStyleBackColor = true;
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.Location = new System.Drawing.Point(392, 179);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 10;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::br.com.franciscorodrigues.mestrado.ETL.Icones.database;
            this.pictureBox1.Location = new System.Drawing.Point(474, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(81, 73);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // pnlServerTxt
            // 
            this.pnlServerTxt.Controls.Add(this.tbxServer);
            this.pnlServerTxt.Controls.Add(this.btnFolder);
            this.pnlServerTxt.Location = new System.Drawing.Point(79, 79);
            this.pnlServerTxt.Name = "pnlServerTxt";
            this.pnlServerTxt.Size = new System.Drawing.Size(469, 21);
            this.pnlServerTxt.TabIndex = 2;
            // 
            // tbxServer
            // 
            this.tbxServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpProvider1.SetHelpKeyword(this.tbxServer, "ConfigDataBase");
            this.helpProvider1.SetHelpString(this.tbxServer, "Caminho do banco de dados ou o endereço do servidor. Exemplo para salvar o banco " +
        "de dados no diretório da aplicação: |DataDirectory|");
            this.tbxServer.Location = new System.Drawing.Point(0, 0);
            this.tbxServer.Name = "tbxServer";
            this.helpProvider1.SetShowHelp(this.tbxServer, true);
            this.tbxServer.Size = new System.Drawing.Size(440, 20);
            this.tbxServer.TabIndex = 0;
            this.tbxServer.Text = "127.0.0.1";
            // 
            // btnFolder
            // 
            this.btnFolder.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnFolder.Image = global::br.com.franciscorodrigues.mestrado.ETL.Properties.Resources.page_white_find;
            this.btnFolder.Location = new System.Drawing.Point(440, 0);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(29, 21);
            this.btnFolder.TabIndex = 1;
            this.btnFolder.UseVisualStyleBackColor = true;
            this.btnFolder.Visible = false;
            this.btnFolder.Click += new System.EventHandler(this.btnFolder_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 214);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(560, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel.Text = "Aperte F1 para ajuda.";
            // 
            // SettingDataBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 236);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pnlServerTxt);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.btnContinuar);
            this.Controls.Add(this.lblDataBase);
            this.Controls.Add(this.tbxDataBase);
            this.Controls.Add(this.lblServer);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.lblSenha);
            this.Controls.Add(this.tbxUser);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.gbxType);
            this.Name = "SettingDataBase";
            this.Text = "Configuração de banco de dados";
            this.Load += new System.EventHandler(this.SettingDataBase_Load);
            this.gbxType.ResumeLayout(false);
            this.gbxType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlServerTxt.ResumeLayout(false);
            this.pnlServerTxt.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxType;
        private System.Windows.Forms.RadioButton rbtLocal;
        private System.Windows.Forms.RadioButton rbtServer;
        private System.Windows.Forms.Label lblDataBase;
        private System.Windows.Forms.TextBox tbxDataBase;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.TextBox tbxPassword;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.TextBox tbxUser;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Button btnContinuar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlServerTxt;
        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.TextBox tbxServer;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
    }
}