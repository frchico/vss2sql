﻿using br.com.franciscorodrigues.mestrado.ETL.Controller;
using br.com.franciscorodrigues.mestrado.ETL.Events;
using br.com.franciscorodrigues.mestrado.ETL.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace br.com.franciscorodrigues.mestrado.ETL.View
{
    public partial class ImportForm : Form
    {
        Context contexto = new Context();
        public ImportForm()
        {
            InitializeComponent();
        }
      
        void vssHelp_Notificar(int qtd, string Mensagem)
        {
            try
            {
                backgroundWorker.ReportProgress(qtd, Mensagem);
            }
            catch
            {
            }
        }

        private void ExtracaoForm_Load(object sender, EventArgs e)
        {
            cmbTipo.DataSource = contexto.ConfigurationTable.ToList();            
            lblMessage.Text = "Status: Waiting command...";

        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var config = e.Argument as ImportarModelView;
           
            ExtractSourceControl extractSourceControl = null;
            if ( config.Configuration.RepoType == "SVN" )
                extractSourceControl = new ExtractFromSVNSourceControl(config.Configuration.RootPath, config.Configuration.TargetPath, config.Configuration.UserName, config.Configuration.Pwd);
            extractSourceControl.ThrowsOnError = false;

            extractSourceControl.OnReportProgress += new ReportProgress(this.ReportProgress);
            
            extractSourceControl.TempPath = config.Configuration.TargetPath;
            extractSourceControl.ImportItemsFromSourceControl(config.From, config.To);
        }
        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnExecutar.Enabled = true;
            Cursor = Cursors.Default;
            if (e.Error != null)
            {
                ReportProgress(e.Error.ToString());
            }
            AtualizarHoraImportacao();
        }
        private class ImportarModelView
        {
            public Configuration Configuration { get; set; }
            public DateTime? From { get; set; }
            public DateTime? To { get; set; }
        }
        private void btnExecutar_Click(object sender, EventArgs e)
        {
            var ImportarModelView = new ImportarModelView();
            if (dtpBegin.Checked)
                ImportarModelView.From = dtpBegin.Value;
            if (dtpEnd.Checked)
                ImportarModelView.To = dtpEnd.Value;
            ImportarModelView.Configuration = cmbTipo.SelectedItem as Configuration;            
            Cursor = Cursors.WaitCursor;
            btnExecutar.Enabled = false;
            backgroundWorker.RunWorkerAsync(ImportarModelView);
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker.CancellationPending)
                this.Close();
        }

        private void ReportProgress(string message)
        {
            if (InvokeRequired)
            {
                lblMessage.Invoke(new ReportProgress(ReportProgress), message);
                return;
            }
            else
                lblMessage.Text = message;
        }
        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ReportProgress(e.UserState.ToString());
        }

        private void cmbTipo_Format(object sender, ListControlConvertEventArgs e)
        {
            var r = ((Configuration)e.ListItem);
            e.Value = r.Nome;
        }

        private void cmbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            AtualizarHoraImportacao();
        }

        private void AtualizarHoraImportacao()
        {
            var r = cmbTipo.SelectedItem as Configuration;
            dtpBegin.Value = contexto.ObterProximaDataImportacao(r.RootPath) ?? DateTime.Now.AddHours(-1);
            dtpEnd.Value = DateTime.Now;
        }

        private void ImportForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.backgroundWorker.CancelAsync();
        }
    }
}
