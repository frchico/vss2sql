using System;
using System.Collections.Generic;
using System.Text;

namespace br.com.franciscorodrigues.mestrado.ETL.Events
{
    public class CodeEventArgs : EventArgs
    {
        public CodeEventArgs(string filePath, int itemId)
            : this(filePath)
        {
            this.itemId = itemId;
        }
        public CodeEventArgs(string filePath)
            : this()
        {
            this.filePath = filePath;
        }
        public CodeEventArgs()
            : base()
        {
        }
        private string filePath;

        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }
        bool completed = false;
        public bool Completed
        {
            get { return completed; }
            set { completed = value; }
        }

        int itemId;
        public int ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }

        bool throwsOnError = false;
        public bool ThrowsOnError
        {
            get { return throwsOnError; }
            set { throwsOnError = value; }
        }
        public Exception exception = null;
        public Exception Exception
        {
            get { return exception; }
            set { exception = value; }
        }
        TimeSpan timeOut = new TimeSpan(0, 0, 30);
        public TimeSpan TimeOut
        {
            get { return this.timeOut; }
            set { this.timeOut = value; }
        }
    }

    public class UserEventArgs : EventArgs
    {
        public string UserName {get;set;}
        public string Login{ get; set; }

        public UserEventArgs(string UserName)
        {
            this.UserName = UserName;
            this.Login = UserName;
        }

    }
}
