using System;
using System.Collections.Generic;
using System.Text;

namespace br.com.franciscorodrigues.mestrado.ETL.Events
{
    public delegate void ReportProgress(string Mensagem);
    public delegate void LoadItemCompleteEventArgs(CodeEventArgs e);
    public delegate void LoadUserCompleteEventArgs(UserEventArgs e);
}
