using System;
using System.Data;
using System.Configuration;
using System.Web;
using log4net;

/// <summary>
/// Summary description for LogUtil
/// </summary>
namespace br.com.franciscorodrigues.mestrado
{
    public static class LogUtil
    {
        private static ILog logger;
        public static ILog Logger
        {
            get
            {
                if (logger == null)
                {
                    string configuracao = ConfigurationManager.AppSettings["ChaveLog"];
                    if ( string.IsNullOrEmpty(configuracao) )
                    {
                        configuracao = "LogInFile";
                    }
                    logger = log4net.LogManager.GetLogger( configuracao);
                }
                return logger;
            }
        }

        internal static void Info(object message, Exception exception)
        {
            if ( Logger.IsInfoEnabled )
                Logger.Info(message, exception);
        }
        internal static void Info(object message)
        {
            Info(message, null);
        }
        public static void InfoFormat(string format, params object[] args)
        {
            if (Logger.IsInfoEnabled)
                Logger.InfoFormat(format, args);
        }

        public static void SendMail(string message)
        {
            if ( Logger.IsFatalEnabled )
                Logger.Fatal(message);
        }
        public static void Debug(object message)
        {
            DebugFormat(message.ToString());
        }
        public static void DebugFormat(string format, params object[] args)
        {
            if (Logger.IsDebugEnabled)
                Logger.DebugFormat(format, args);
        }
    }
}